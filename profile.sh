#!/bin/bash
cargo build --release
rm callgrind.out.* -f
valgrind --tool=callgrind ./target/release/max-temporality
cat callgrind.out.* | rustfilt > callgrind.out.0
kcachegrind callgrind.out.0 &
