#!/bin/bash
DIR=$1
RES=500
BATCH=10
DELAY=30

trap exit SIGINT

cd $DIR

if [ `head plot.dot -n 2 | wc -l` -ne 2 ]
then
    exit
fi

echo 'Rendering images...'
dot -Tpng -Kcirco -Nfixedsize=true -Gsize=${RES},${RES}\! -Gdpi=1 -O plot.dot
mv plot.dot.png plot.dot.1.png

for i in `ls plot.dot.*.png`
do
    convert $i -gravity center -background white -extent ${RES}x${RES} $i
done

IMGS=`ls plot.dot.*.png | sort -V`
IMGS=($IMGS)
for (( i=0; $i<${#IMGS[@]}; i+=$BATCH ))
do
    echo "Rendering intermediate gifs... $i / ${#IMGS[@]}"
    convert -delay $DELAY -loop 0 "${IMGS[@]:$i:$BATCH}" animated.$i.gif
done

GIFS=`ls animated.*.gif | sort -V`
convert $GIFS animated.gif
rm $GIFS

echo "Rendering $DIR: done"