//
// Types critiques - possible de les changer pour travailler avec des graphes
// temporels avec plus de sommets
//

pub type Vertex = u8; // Type pour les identifiants de sommets et le nombre de sommets
pub type LabelId = u8; // Pour les identifiants d'étiquettes / leur nombre
pub type LabelTime = u8; // Pour les valeurs des étiquettes / la temporalité

// Structure représentant une arête. On met le plus petit sommet dans le premier champ
#[derive(Clone, Copy, Hash, PartialEq, Eq, Debug)]
pub struct Edge(Vertex, Vertex);

#[derive(Clone, Copy)]
pub struct Label {
    pub id: LabelId,
    pub time: LabelTime,
    pub edge: Edge,
}

#[allow(dead_code)]
impl Edge {
    pub fn new(u: Vertex, v: Vertex) -> Edge {
        if u <= v {
            Edge(u, v)
        } else {
            Edge(v, u)
        }
    }

    pub fn neighbor(self, u: Vertex) -> Vertex {
        if self.0 == u {
            self.1
        } else {
            self.0
        }
    }

    // Renvoie les extrémités de l'arête, le plus petit sommet à gauche
    pub fn ends(self) -> (Vertex, Vertex) {
        (self.0, self.1)
    }

    pub fn adjacent(self, other: Edge) -> bool {
        self.0 == other.0 || self.0 == other.1 || self.1 == other.0 || self.1 == other.1
    }
}
