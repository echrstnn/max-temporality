//
// Module qui permet de créer d'implémenter un ReachabilitySet pour les graphes cycles
//
// La structure CircSegment est un "segment circulaire". On peut maintenir ces objets
// en maintenant seulement les extrémités, comme pour les segments classiques. Un
// certain nombre de choses sont à adapter toutefois. On peut alors calculer l'union de
// deux CircSegment en temps constant (dans l'implémentation on impose que les segments
// dont on calcule l'union soient adjacent ou s'intersectent, afin que leur union soit
// encore un segment)
//
// Les segments circulaires ont étés abandonnés au profit d'opérations bit à bit (un
// simple ou suffit à calculer l'union). Plus rapides, elles permettent une génération
// plus efficace. Il est toutefois possible de décommenter ce module et de reprendre
// l'ancienne implémentation de CycleReachabilitySet pour pouvoir faire des calculs sur
// des TCycleGraph avec un plus grand nombre de sommets.
//

// use std::cmp::{max, min};
// use std::fmt::{Debug, Display};

// #[derive(Clone, Copy, Debug, PartialEq, Eq)]
// pub struct StrictSubset {
//     a: isize,
//     b: isize,
//     n: usize,
// }

// #[derive(Clone, Copy, PartialEq, Eq, Debug)]
// pub enum CircSegment {
//     EmptySet,
//     FullSet(usize),
//     StrictSubset(StrictSubset),
// }

// use CircSegment::*;

// impl Display for CircSegment {
//     fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
//         match *self {
//             EmptySet => write!(f, "x"),
//             FullSet(_) => write!(f, "A"),
//             StrictSubset(subset) => {
//                 let (a, b) = subset.bounds();
//                 if subset.len() == 1 {
//                     write!(f, "{}", a)
//                 } else {
//                     write!(f, "{}..{}", a, b)
//                 }
//             }
//         }
//     }
// }

// impl StrictSubset {
//     pub fn bounds(self) -> (isize, isize) {
//         (self.a, self.b % self.n as isize)
//     }

//     fn len(self) -> isize {
//         self.b - self.a + 1
//     }

//     fn standardize(self) -> CircSegment {
//         let len = self.len();
//         if len <= 0 {
//             EmptySet
//         } else if len >= self.n as isize {
//             FullSet(self.n)
//         } else {
//             let offset = (self.a + self.n as isize) % self.n as isize - self.a;
//             StrictSubset(StrictSubset {
//                 a: self.a + offset,
//                 b: self.b + offset,
//                 n: self.n,
//             })
//         }
//     }

//     fn translate_near(self, other: Self) -> Self {
//         debug_assert_eq!(self.n, other.n);
//         let mut ans = self;
//         let nisz = self.n as isize;
//         if self.b < other.a - 1 {
//             ans.a += nisz;
//             ans.b += nisz;
//         } else if other.b < self.a - 1 {
//             ans.a -= nisz;
//             ans.b -= nisz;
//         }
//         debug_assert!(!(ans.b < other.a - 1 || other.b < ans.a - 1));
//         ans
//     }

//     fn union(self, other: Self) -> CircSegment {
//         let self_tr = self.translate_near(other);
//         StrictSubset {
//             a: min(self_tr.a, other.a),
//             b: max(self_tr.b, other.b),
//             n: self.n,
//         }
//         .standardize()
//     }
// }

// #[allow(dead_code)]
// impl CircSegment {
//     pub fn singleton(elt: isize, n: usize) -> CircSegment {
//         Self::forward(elt, elt, n)
//     }

//     pub fn forward(begin: isize, end: isize, n: usize) -> CircSegment {
//         debug_assert!(0 <= begin && begin <= end + 1 && end < n as isize);
//         StrictSubset {
//             a: begin,
//             b: end,
//             n,
//         }
//         .standardize()
//     }

//     pub fn backward(begin: isize, end: isize, n: usize) -> CircSegment {
//         debug_assert!(0 <= end && end < begin && begin < n as isize);
//         StrictSubset {
//             a: begin,
//             b: end + n as isize,
//             n,
//         }
//         .standardize()
//     }

//     pub fn len(self) -> usize {
//         match self {
//             EmptySet => 0,
//             FullSet(n) => n,
//             StrictSubset(subset) => subset.len() as usize,
//         }
//     }

//     pub fn contains(self, elt: isize) -> bool {
//         match self {
//             EmptySet => false,
//             FullSet(_) => true,
//             StrictSubset(StrictSubset { a, b, n }) => {
//                 let mut elt = elt;
//                 if elt < a {
//                     elt += n as isize
//                 } else if b < elt {
//                     elt -= n as isize
//                 }
//                 !(elt < a || b < elt)
//             }
//         }
//     }

//     pub fn contains_all(self) -> bool {
//         match self {
//             FullSet(_) => true,
//             _ => false,
//         }
//     }

//     pub fn union(self, other: Self) -> CircSegment {
//         match (self, other) {
//             (EmptySet, _) => other,
//             (_, EmptySet) => self,
//             (FullSet(_), _) => self,
//             (_, FullSet(_)) => other,
//             (StrictSubset(self_), StrictSubset(other)) => self_.union(other),
//         }
//     }
// }

// #[test]
// fn test() {
//     debug_assert_eq!(CircSegment::forward(0, 4, 5), FullSet(5));
//     debug_assert_eq!(CircSegment::forward(1, 0, 9), EmptySet);
//     debug_assert_eq!(CircSegment::backward(1, 0, 9), FullSet(9));
//     debug_assert_eq!(CircSegment::backward(9, 1, 10).len(), 3);

//     let a = CircSegment::forward(2, 8, 10);
//     let b = CircSegment::forward(4, 6, 10);
//     let c = CircSegment::backward(9, 1, 10);
//     let d = CircSegment::backward(6, 0, 10);
//     debug_assert_eq!(a.len(), 7);
//     debug_assert_eq!(d.len(), 5);

//     debug_assert_eq!(a.union(a), a);
//     debug_assert_eq!(a.union(b), a);
//     debug_assert_eq!(a.union(c), FullSet(10));
//     debug_assert_eq!(c.union(d), CircSegment::backward(6, 1, 10));
//     debug_assert_eq!(b.union(c.union(d)), CircSegment::backward(4, 1, 10));
//     debug_assert_eq!(d.union(d), d);
// }
