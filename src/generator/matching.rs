use super::GeneratorThread;
use crate::{generic, Callback, Edge};
use rustc_hash::{FxHashMap, FxHashSet};
use std::{hash::Hash, sync::Arc};

// Exponentiation rapide pour le calcul de (x^p) % MOD
fn mod_pow(x: u64, p: u64) -> u64 {
    if p == 0 {
        x % Matching::MOD
    } else if p % 2 == 0 {
        mod_pow((x * x) % Matching::MOD, p / 2)
    } else {
        (x * mod_pow((x * x) % Matching::MOD, p / 2)) % Matching::MOD
    }
}

#[derive(Clone)]
pub struct Matching {
    edges_set: FxHashSet<Edge>,
    edges_vec: Vec<Edge>,
    hash: u64,
}

impl Hash for Matching {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.hash.hash(state);
    }
}

impl PartialEq for Matching {
    fn eq(&self, other: &Self) -> bool {
        self.hash == other.hash && self.edges_set == other.edges_set
    }
}

impl Eq for Matching {}

impl Matching {
    const MOD: u64 = 1_000_000_007;

    pub fn new() -> Self {
        Self {
            edges_set: FxHashSet::default(),
            edges_vec: vec![],
            hash: 0,
        }
    }

    pub fn iter(&self) -> impl Iterator<Item = &Edge> {
        self.edges_vec.iter()
    }

    pub fn edges_vec(&self) -> &Vec<Edge> {
        &self.edges_vec
    }

    pub fn is_empty(&self) -> bool {
        self.edges_vec.is_empty()
    }

    pub fn contains(&self, uv: Edge) -> bool {
        self.edges_set.contains(&uv)
    }

    pub fn is_subset(&self, set: &FxHashSet<Edge>) -> bool {
        self.edges_set.is_subset(set)
    }

    pub fn insert(&mut self, cache: &MatchingCache, uv: Edge) {
        self.edges_set.insert(uv);
        self.edges_vec.push(uv);
        self.hash = (self.hash + cache.edge_contrib(uv)) % Self::MOD;
    }

    pub fn remove(&mut self, cache: &MatchingCache, uv: Edge) {
        self.edges_set.remove(&uv);
        let pop = self.edges_vec.pop().unwrap();
        debug_assert!(uv == pop);
        self.hash = ((Self::MOD + self.hash) - cache.edge_contrib(uv)) % Self::MOD;
    }
}

#[derive(Clone)]
pub struct MatchingCache {
    n: u64,
    edge_contrib: Vec<u64>,
    all_matchings: Vec<Matching>,
    matching_index: FxHashMap<Matching, usize>,
}

impl MatchingCache {
    pub fn new() -> Self {
        Self {
            n: 0,
            edge_contrib: vec![],
            all_matchings: vec![],
            matching_index: FxHashMap::default(),
        }
    }

    pub fn is_computed(&self) -> bool {
        self.n != 0
    }

    pub fn all_matchings(&self) -> &[Matching] {
        &self.all_matchings
    }

    pub fn matching_index(&self, matching: &Matching) -> usize {
        *self.matching_index.get(&matching).unwrap()
    }

    pub fn edge_contrib(&self, uv: Edge) -> u64 {
        let (u, v) = uv.ends();
        let (u, v) = (u as u64, v as u64);
        let index = u * self.n + v;
        debug_assert!(self.is_computed());
        debug_assert!(self.edge_contrib[index as usize] != 0);
        self.edge_contrib[index as usize]
    }

    fn compute_edge_contrib(&mut self, uv: Edge) {
        let (u, v) = uv.ends();
        let (u, v) = (u as u64, v as u64);
        let index = u * self.n + v;
        self.edge_contrib[index as usize] = mod_pow(2, index);
    }
}

impl<F, D, RS> GeneratorThread<F, D, RS>
where
    F: Callback<RS>,
    D: Callback<RS>,
    RS: generic::ReachabilitySet,
{
    pub fn compute_mcache(&mut self)
    where
        F: Callback<RS>,
        D: Callback<RS>,
        RS: generic::ReachabilitySet,
    {
        debug_assert!(!self.mcache.is_computed());
        let g = &self.G;
        let n = g.vertices().len();
        let mcache = self.uncomputed_mcache.as_mut().unwrap();
        mcache.n = n as u64;
        mcache.edge_contrib = vec![0; n * n];

        let edges: Vec<Edge> = g.edges().cloned().collect();
        edges
            .iter()
            .for_each(|uv| mcache.compute_edge_contrib(*uv));
        self.mcache = Arc::new(mcache.clone());

        self.for_each_matching(edges, |self_, matching| {
            let mcache = self_.uncomputed_mcache.as_mut().unwrap();
            mcache
                .matching_index
                .insert(matching.clone(), mcache.all_matchings.len());
            mcache.all_matchings.push(matching.clone());
        });
        let mcache = self.uncomputed_mcache.take().unwrap();
        self.mcache = Arc::new(mcache);
    }
}
