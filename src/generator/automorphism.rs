use super::{matching::Matching, GeneratorThread};
use crate::{generic, Callback, Edge, Vertex};
use rustc_hash::FxHashSet;
use std::ops::Index;

// Structure représentant un automorphisme
// self.0[i]=j signifie f(i)=j
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Automorphism(Vec<Vertex>);

impl Automorphism {
    pub fn new(n: Vertex, aut: Vec<Vertex>) -> Self {
        debug_assert!(n as usize == aut.len());
        debug_assert!(
            (0..n).collect::<FxHashSet<_>>() == aut.iter().cloned().collect::<FxHashSet<_>>()
        );
        Self(aut)
    }
}

impl Index<Vertex> for Automorphism {
    type Output = Vertex;

    fn index(&self, index: Vertex) -> &Self::Output {
        self.0.index(index as usize)
    }
}

impl<F, D, RS> GeneratorThread<F, D, RS>
where
    F: Callback<RS>,
    D: Callback<RS>,
    RS: generic::ReachabilitySet,
{
    // Opération à effectuer après chaque ajout d'un couplage sur le graphe et qui sert
    // à retenir parmis les anciens automorphismes ceux qui sont toujours des
    // automorphismes après l'ajout du couplage. Étant donné qu'on exécute cette
    // opération après chaque couplage, un ancien automorphisme l'est encore ssi il est
    // un morphisme sur le couplage. [=> ok et <= tout morphisme du couplage dans lui
    // même est alors un automorphisme du couplage dans lui même. f est donc un
    // automorphisme du couplage uniquement mais également du graphe sans le couplage.
    // On a bien u-v arête ssi f(u)-f(v)} arête en examinant le cas où u-v est dans le
    // couplage et celui où u-v est dans l'ancien graphe]
    pub fn retain_auts(&mut self, matching: &Matching) {
        debug_assert!(!self.auts.last().unwrap().is_empty());
        let new_auts = self
            .auts
            .last()
            .unwrap()
            .iter()
            .filter(|&aut| {
                matching.iter().all(|&uv| {
                    let (u, v) = uv.ends();
                    let u_by_aut = aut[u];
                    let v_by_aut = aut[v];
                    let uv_by_aut = Edge::new(u_by_aut, v_by_aut);
                    matching.contains(uv_by_aut)
                })
            })
            .cloned()
            .collect();
        self.auts.push(new_auts);
    }

    // Opération duale de retain_auts.
    pub fn restore_auts(&mut self) {
        self.auts.pop();
    }
}
