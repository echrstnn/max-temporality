use super::{matching::Matching, Callback, GeneratorThread};
use crate::{generic, Edge};
use std::sync::Arc;

// Variables à transmettre dans les appels récursifs de for_each_matching_
struct MatchingGenGlobals {
    // Si un sommet ou non a une arête adjacente dans le couplage ou non
    free_vertex: Vec<bool>,

    // Le couplage en cours de formation
    matching: Matching,
}

impl<F, D, RS> GeneratorThread<F, D, RS>
where
    F: Callback<RS>,
    D: Callback<RS>,
    RS: generic::ReachabilitySet,
{
    pub fn for_each_matching_up_to_iso<T>(&mut self, eligible: Vec<Edge>, f: T)
    where
        T: FnMut(&mut Self, &Matching),
    {
        // Si il y a des symétries
        if self.auts.last().unwrap().len() > 1 {
            // On va utiliser le cache où tous les couplages sont présent, on en gardera
            // qu'un par classe d'isomorphisme et on utilisera push_label pour ajouter
            // uniquement les étiquettes des couplages conservés
            self.for_each_matching_using_cache(eligible, f);
        } else {
            // On génère les couplages à la volée, on ne génèrera pas de couplages
            // isomorphes car il n'y a pas de symétrie. Cela permet de faire moins
            // d'appels coûteux à push_label
            self.for_each_matching(eligible, f);
        }
    }

    // Exécute la cloture f sur tous les couplages des arêtes éligibles
    // Utile pour la génération des couplages pour le calcul du cache et égelement
    // la génération des graphes dans le cas où il n'y a pas de symétries
    pub fn for_each_matching<T>(&mut self, eligible: Vec<Edge>, mut f: T)
    where
        T: FnMut(&mut Self, &Matching),
    {
        let mut globals = MatchingGenGlobals {
            free_vertex: vec![true; self.G.vertices().len()],
            matching: Matching::new(),
        };
        self.for_each_matching_(&mut globals, &mut f, &eligible[..])
    }

    // Fonction auxliaire pour la génération des couplages
    fn for_each_matching_<T>(
        &mut self,
        globals: &mut MatchingGenGlobals,
        f: &mut T,
        remaining: &[Edge],
    ) where
        T: FnMut(&mut Self, &Matching),
    {
        // Si G n'est pas minimal on peut directement couper la branche
        if self.G.is_minimal() {
            // Si reste des arêtes à potentiellement ajouter
            if let Some((&uv, wo_uv)) = remaining.split_first() {
                let (u, v) = uv.ends();
                if globals.free_vertex[u as usize] && globals.free_vertex[v as usize] {
                    globals.free_vertex[u as usize] = false;
                    globals.free_vertex[v as usize] = false;
                    globals.matching.insert(&self.mcache, uv);
                    self.G.push_label(uv);
                    self.debug();

                    self.for_each_matching_(globals, f, wo_uv);

                    self.G.pop_label();
                    self.debug();
                    globals.matching.remove(&self.mcache, uv);
                    globals.free_vertex[u as usize] = true;
                    globals.free_vertex[v as usize] = true;
                }

                self.for_each_matching_(globals, f, wo_uv);
            }
            // Sinon si le couplage est non vide
            else if !globals.matching.is_empty() {
                self.G.incr_time();
                f(self, &globals.matching);
                self.G.decr_time();
            }
        }
    }

    // Gérère les couplages sur les arêtes éligibles en utilisant le cache.
    // Utilise le cache où tous les couplages sont présent, conserve un couplage par
    // classe d'isomorphisme puis utilise push_label pour ajouter uniquement les
    // étiquettes des couplages conservés
    #[inline(never)]
    fn for_each_matching_using_cache<T>(&mut self, eligible: Vec<Edge>, mut f: T)
    where
        T: FnMut(&mut Self, &Matching),
    {
        debug_assert!(self.mcache.is_computed());
        let eligible_set = eligible.iter().cloned().collect();
        let mcache = Arc::clone(&self.mcache);
        let all_matchings = mcache.all_matchings();
        let mut seen_up_to_iso = vec![false; all_matchings.len()];

        for (i, matching) in all_matchings.iter().enumerate() {
            if !seen_up_to_iso[i] && matching.is_subset(&eligible_set) {
                let mut added_labels = 0;
                if matching.iter().any(|&uv| {
                    self.G.push_label(uv);
                    self.debug();
                    added_labels += 1;
                    !self.G.is_minimal()
                }) {
                    for _ in 0..added_labels {
                        self.G.pop_label()
                    }
                } else {
                    if self.G.is_minimal() {
                        self.mark_as_seen(&mut seen_up_to_iso, matching);
                        self.G.incr_time();
                        self.retain_auts(matching);

                        f(self, matching);

                        self.restore_auts();
                        self.G.decr_time();
                    }

                    matching.iter().for_each(|_| self.G.pop_label());
                    self.debug();
                }
            }
        }
    }

    fn mark_as_seen(&mut self, seen_up_to_iso: &mut Vec<bool>, matching: &Matching) {
        self.auts.last().unwrap().iter().for_each(|aut| {
            let mut by_aut = Matching::new();
            matching.iter().for_each(|&uv| {
                let (u, v) = uv.ends();
                let uv_by_aut = Edge::new(aut[u], aut[v]);
                by_aut.insert(&self.mcache, uv_by_aut);
            });
            seen_up_to_iso[self.mcache.matching_index(&by_aut)] = true;
        });
    }
}
