use super::{matching::MatchingCache, Callback, GeneratorThread, FORK_DEPTH};
use crate::{generic, Edge, Generator, Vertex};
use std::sync::Arc;

impl<F, D, RS> GeneratorThread<F, D, RS>
where
    F: Callback<RS>,
    D: Callback<RS>,
    RS: generic::ReachabilitySet,
{
    pub fn initial_thread(globals: Arc<Generator<F, D, RS>>, debug: bool) -> Self {
        debug_assert!(globals.initial.labels().next().is_none());
        let mut self_ = Self {
            G: globals.initial.clone(),
            auts: vec![RS::automorphisms(globals.initial.vertices().len() as Vertex)],
            id: 1,
            depth: 1,
            mcache: Arc::new(MatchingCache::new()),
            uncomputed_mcache: Some(MatchingCache::new()),
            globals,
            debug,
        };
        self_.compute_mcache();
        self_
    }

    pub fn fork(&mut self) -> Self {
        debug_assert!(self.mcache.is_computed() && self.uncomputed_mcache.is_none());
        self.globals.stats.lock().unwrap().tot_tasks += 1;
        let id;
        {
            let mut next_id = self.globals.next_id.lock().unwrap();
            id = *next_id;
            *next_id += 1;
        }
        Self {
            G: self.G.clone(),
            auts: vec![self.auts.last().unwrap().clone()],
            id,
            depth: self.depth + 1,
            mcache: Arc::clone(&self.mcache),
            uncomputed_mcache: None,
            globals: Arc::clone(&self.globals),
            debug: false,
        }
    }

    pub fn callback(&mut self) {
        (self.globals.callback.lock().unwrap())(&self.G);
    }

    pub fn debug(&mut self) {
        if self.debug {
            (self.globals.debug_callback.lock().unwrap())(&self.G);
        }
    }

    pub fn generate(&mut self, recent: &[Edge]) {
        if self.G.is_minimal() {
            if self.G.is_temporally_connected() {
                self.globals.stats.lock().unwrap().generated += 1;
                self.callback();
            } else {
                let mut eligible = recent
                    .iter()
                    .flat_map(|&uv| {
                        let (u, v) = uv.ends();
                        self.G.adj_edges(u).chain(self.G.adj_edges(v)).cloned()
                    })
                    .filter(|&uv| {
                        let (u, v) = uv.ends();
                        self.G.reachability_set(u) != self.G.reachability_set(v)
                    })
                    .collect::<Vec<_>>();
                eligible.sort_unstable_by(|e1, e2| {
                    let (u1, v1) = e1.ends();
                    let (u2, v2) = e2.ends();
                    if u1 == u2 {
                        v1.cmp(&v2)
                    } else {
                        u1.cmp(&u2)
                    }
                });
                eligible.dedup();
                if self.depth <= FORK_DEPTH {
                    // On fork vers de nouvelles tâches
                    rayon::scope(move |s| {
                        self.for_each_matching_up_to_iso(eligible, |self_, matching| {
                            let mut child = self_.fork();
                            let matching = matching.iter().cloned().collect::<Vec<_>>();
                            let parent_id = self_.id;
                            println!("[ Task {: ^4}] Forking to task {}", parent_id, child.id);
                            s.spawn(move |_| {
                                println!(
                                    "[ Task {: ^4}] Started from task {}",
                                    child.id, parent_id
                                );
                                child.generate(&matching)
                            });
                        });
                        println!("[ Task {: ^4}] Is waiting for children tasks", self.id);
                    })
                } else {
                    // On continue dans ce thread
                    self.for_each_matching_up_to_iso(eligible, |self_, matching| {
                        self_.generate(matching.edges_vec())
                    });
                }
            }
        }
    }
}

impl<F, D, RS> Drop for GeneratorThread<F, D, RS>
where
    F: Callback<RS>,
    D: Callback<RS>,
    RS: generic::ReachabilitySet,
{
    fn drop(&mut self) {
        println!("[ Task {: ^4}] Finished", self.id);
        self.globals.stats.lock().unwrap().completed_tasks += 1;
    }
}
