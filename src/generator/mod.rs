use crate::generic::{self};
use std::{
    sync::{Arc, Mutex},
    thread,
    time::Duration,
};

mod automorphism;
pub mod generator_thread;
pub mod matching;
pub mod matching_gen;

pub use automorphism::Automorphism;
use matching::MatchingCache;

// Profondeur d'exploration jusqu'à laquelle on crée une nouvelle tâche
const FORK_DEPTH: usize = 2;

// Raccourci pour ne pas avoir à tout réécrire à chaque fois
pub trait Callback<RS: generic::ReachabilitySet>:
    FnMut(&generic::TGraph<RS>) + Send + 'static
{
}
impl<T, RS: generic::ReachabilitySet> Callback<RS> for T where
    T: FnMut(&generic::TGraph<RS>) + Send + 'static
{
}

// Statistiques sur la génération
pub struct Generator<F, D, RS>
where
    F: Callback<RS>,
    D: Callback<RS>,
    RS: generic::ReachabilitySet,
{
    // Le prochain identifiant de tâche à attribuer
    next_id: Mutex<usize>,

    // Stats diverses sur la génération
    stats: Arc<Mutex<GeneratorStats>>,

    // Les callbacks
    callback: Mutex<F>,
    debug_callback: Mutex<D>,

    // Graphe initial
    initial: generic::TGraph<RS>,
}

struct GeneratorStats {
    generated: usize,
    tot_tasks: usize,
    waiting_tasks: usize,
    completed_tasks: usize,
}

// Structure sur laquelle travaille un thread. Chaque thread possède une instance qui
// lui est propre, il n'y a pas de partage entre threads autre que les callbacks
#[allow(non_snake_case)]
struct GeneratorThread<F, D, RS>
where
    F: Callback<RS>,
    D: Callback<RS>,
    RS: generic::ReachabilitySet,
{
    // Le graphe sur lequel on travaille, son état actuel
    G: generic::TGraph<RS>,

    // Pile dont le sommet est l'ensemble des éléments du groupe des automorphismes de G
    auts: Vec<Vec<Automorphism>>,

    // L'identifiant de la tâche associée
    id: usize,

    // Profondeur d'exploration actuelle (sert à décider de fork ou non)
    depth: usize,

    // Le cache pour itérer rapidement sur les couplages
    mcache: Arc<MatchingCache>,

    // Some(_) quand le mcache n'est pas calculé. Le mcache sera calculé dans ce champ
    // puis transféré dans mcache et ce champ sera mis à None. Cette astuce permet de ne
    // pas avoir un mutex sur mcache qui empêcherait véritablement plusieurs threads de
    // le lire simultanément (ou évite le surcoût lié à un RwLock)
    uncomputed_mcache: Option<MatchingCache>,

    // L'objet global Generator
    globals: Arc<Generator<F, D, RS>>,

    // Si le callback de debug doit être appelé
    debug: bool,
}

impl<F, D, RS> Generator<F, D, RS>
where
    F: Callback<RS>,
    D: Callback<RS>,
    RS: generic::ReachabilitySet,
{
    pub fn for_each_min_spanner(
        initial: generic::TGraph<RS>,
        callback: F,
        debug_callback: D,
        debug: bool,
    ) -> usize
    where
        F: Callback<RS>,
        D: Callback<RS>,
        RS: generic::ReachabilitySet,
    {
        println!("[{: ^10}] Generator starting...", "Main");

        let edges = initial.edges().cloned().collect::<Vec<_>>();
        let globals = Arc::new(Self {
            next_id: Mutex::new(1),
            stats: Arc::new(Mutex::new(GeneratorStats {
                generated: 0,
                tot_tasks: 1,
                waiting_tasks: 0,
                completed_tasks: 0,
            })),
            callback: Mutex::new(callback),
            debug_callback: Mutex::new(debug_callback),
            initial,
        });
        globals.spawn_status_thread();

        GeneratorThread::initial_thread(Arc::clone(&globals), debug).generate(&edges);

        let generated = globals.stats.lock().unwrap().generated;
        generated
    }

    fn spawn_status_thread(&self) {
        let stats = Arc::clone(&self.stats);
        thread::spawn(move || {
            const SLEEP: u64 = 10;
            thread::sleep(Duration::from_secs(SLEEP));
            let mut finished = false;
            while !finished {
                let completed_tasks = stats.lock().unwrap().completed_tasks;
                let tot_tasks = stats.lock().unwrap().tot_tasks;
                println!(
                    "[{: ^10}] {} (+{}) / {} completed tasks, {} min spanners generated",
                    "Main",
                    completed_tasks,
                    stats.lock().unwrap().waiting_tasks,
                    tot_tasks,
                    stats.lock().unwrap().generated
                );
                thread::sleep(Duration::from_secs(SLEEP));
                finished = completed_tasks == tot_tasks;
            }
        });
    }
}
