use std::sync::{Arc, Mutex};

use rustc_hash::FxHashMap;
use tgraphs::*;

fn main() {
    let n: Vertex = 6;

    let with_temp: FxHashMap<LabelTime, usize> = (0..n).map(|i| (i as LabelTime, 0)).collect();
    let with_temp = Arc::new(Mutex::new(with_temp));

    let generated: usize;

    {
        let with_temp = Arc::clone(&with_temp);

        // let mut plot = TGraphPlotter::new(&format!("plots_tmax_{}", n));
        let g = TGraph::new(n);

        let mut max_temp = 0;
        let callback = move |g: &TGraph| {
            *with_temp.lock().unwrap().get_mut(&g.temporality()).unwrap() += 1;
            let g_temp = g.temporality();

            if max_temp < g_temp {
                max_temp = g_temp;
                println!(
                    "[{: ^10}] Found min spanner with temporality {}",
                    "FOUND", g_temp
                );
            }
            // if g_temp >= (n + 1) / 2 {
            //     plot.plot(&g);
            // }
        };

        let debug_callback = |_g: &TGraph| {};
        generated = Generator::for_each_min_spanner(g, callback, debug_callback, false);
    }

    let mut with_temp = with_temp
        .lock()
        .unwrap()
        .iter()
        .filter_map(|(i, j)| if *j > 0 { Some((*i, *j)) } else { None })
        .collect::<Vec<_>>();
    with_temp.sort_unstable_by_key(|(i, _)| *i);

    println!("\n\n\nTOTAL for n={}: {}", n, generated);
    println!("Details: {:?}", with_temp);
}
