use crate::{generic::ReachabilitySet, Label, LabelId, Vertex};

// Structure qui facilite l'écriture du code
#[derive(Clone)]
struct VertexReachabilites<RS: ReachabilitySet> {
    // Pile dont le sommet est l'ensemble des sommets pouvant accéder à u
    can_reach: Vec<RS>,

    // Pile dont le sommet top est tel que top[label_id] est l'ensemble des sommets
    // pouvant accéder à u sans utiliser le label d'id label_id
    can_reach_wo: Vec<Box<[RS]>>,
}

// Structure qui maintient les accessibilités et permet les tests de connexité
// et minimalités en temps constant moyennant un surcoût en O(labels.len()) lors
// de push_label et pop_label
#[derive(Clone)]
pub struct Reachabilities<RS: ReachabilitySet> {
    // Structure qui facilite l'écriture du code
    vertex_reachabilities: Box<[VertexReachabilites<RS>]>,

    // Pile dont le sommet top est tel que top[label_id] indique combien de sommets
    // peuvent être accédés par un ensemble de sommets différent si l'on supprime le
    // label label_id
    reach_diff_wo: Vec<Box<[Vertex]>>,

    // Pile dont le sommet indique combien de labels sont inutiles, i.e. dont la
    // suppression ne rompt aucun trajet et également le nombre de 0 dans le sommet de
    // reach_diff_wo
    useless_labels: Vec<LabelId>,

    // Pile dont le sommet indique combine de sommets ne peuvent pas être accédés par
    // tous les autres
    not_fully_reachable: Vec<Vertex>,

    // Piles depuis lesquelles on prend les plages de mémoire à ajouter et dans
    // lesquelles on ajoute celles qui ont étées supprimées pour éviter de
    // réallouer et libérer des grosses plages à chaque push/pop_label
    mem_reuse_brs: Vec<Box<[RS]>>,
    mem_reuse_bvertex: Vec<Box<[Vertex]>>,
}

impl<RS: ReachabilitySet> Reachabilities<RS> {
    pub fn new(n: Vertex) -> Self {
        debug_assert!(n >= 2);
        let n_us = n as usize;
        let max_label_id = (n_us * n_us * (n_us - 1)) / 2;
        let mut mem_reuse_brs = vec![vec![RS::singleton(0, n); max_label_id].into_boxed_slice()];
        let mut mem_reuse_bvertex = vec![vec![0; max_label_id].into_boxed_slice()];
        let vertex_reachabilities = (0..n)
            .map(|u| VertexReachabilites {
                can_reach: vec![RS::singleton(u, n)],
                can_reach_wo: vec![Self::alloc_brs(&mut mem_reuse_brs)],
            })
            .collect::<Vec<_>>()
            .into_boxed_slice();
        let reach_diff_wo = vec![Self::alloc_bvertex(&mut mem_reuse_bvertex)];
        Self {
            vertex_reachabilities,
            reach_diff_wo,
            useless_labels: vec![0],
            not_fully_reachable: vec![n],
            mem_reuse_brs,
            mem_reuse_bvertex,
        }
    }

    // Renvoie l'ensemble des sommets pouvant accéder à u
    pub fn reachability_set(&self, u: Vertex) -> &RS {
        self.vertex_reachabilities[u as usize]
            .can_reach
            .last()
            .unwrap()
    }

    pub fn is_useless_label(&self, l: Label) -> bool {
        self.reach_diff_wo.last().unwrap()[l.id as usize] == 0
    }

    pub fn is_temporally_connected(&self) -> bool {
        *self.not_fully_reachable.last().unwrap() == 0
    }

    pub fn is_minimal(&self) -> bool {
        *self.useless_labels.last().unwrap() == 0
    }

    #[inline(always)]
    fn alloc_brs(mem_reuse_brs: &mut Vec<Box<[RS]>>) -> Box<[RS]> {
        let ans = mem_reuse_brs.pop().unwrap();
        if mem_reuse_brs.is_empty() {
            mem_reuse_brs.push(ans.clone());
        }
        ans
    }

    #[inline(always)]
    fn alloc_bvertex(mem_reuse_bvertex: &mut Vec<Box<[Vertex]>>) -> Box<[Vertex]> {
        let ans = mem_reuse_bvertex.pop().unwrap();
        if mem_reuse_bvertex.is_empty() {
            mem_reuse_bvertex.push(ans.clone());
        }
        ans
    }

    pub fn push_label(&mut self, newl: Label) {
        let (u, v) = newl.edge.ends();

        self.vertex_reachabilities.iter_mut().for_each(|w_rties| {
            w_rties.can_reach_wo.last_mut().unwrap()[newl.id as usize] =
                w_rties.can_reach.last().unwrap().clone()
        });

        let (u_rties, v_rties);
        {
            let (left, right) = self.vertex_reachabilities.split_at_mut(v as usize);
            u_rties = &mut left[u as usize];
            v_rties = &mut right[0];
        }

        let can_reach_u = u_rties.can_reach.last().unwrap();
        let can_reach_v = v_rties.can_reach.last().unwrap();
        let union = can_reach_u.union(can_reach_v);

        let can_reach_u_wo = u_rties.can_reach_wo.last().unwrap().as_ref();
        let can_reach_v_wo = v_rties.can_reach_wo.last().unwrap().as_ref();
        let reach_diff_wo = self.reach_diff_wo.last().unwrap().as_ref();
        let mut new_can_reach_u_wo = Self::alloc_brs(&mut self.mem_reuse_brs);
        let mut new_can_reach_v_wo = Self::alloc_brs(&mut self.mem_reuse_brs);
        let mut new_reach_diff_wo = Self::alloc_bvertex(&mut self.mem_reuse_bvertex);
        let mut new_useless_labels = *self.useless_labels.last().unwrap();
        let mut new_not_fully_reachable = *self.not_fully_reachable.last().unwrap();

        (0..newl.id).for_each(|l| {
            let can_reach_u_wo_l = &can_reach_u_wo[l as usize];
            let can_reach_v_wo_l = &can_reach_v_wo[l as usize];
            let union_wo_l = can_reach_u_wo_l.union(can_reach_v_wo_l);

            let reach_diff_wo_l = reach_diff_wo[l as usize];
            let new_reach_diff_wo_l = (reach_diff_wo_l + 2 * (union != union_wo_l) as Vertex)
                - (can_reach_u != can_reach_u_wo_l) as Vertex
                - (can_reach_v != can_reach_v_wo_l) as Vertex;
            new_useless_labels += (new_reach_diff_wo_l == 0 && reach_diff_wo_l != 0) as LabelId;

            new_can_reach_u_wo[l as usize] = union_wo_l.clone();
            new_can_reach_v_wo[l as usize] = union_wo_l;
            new_reach_diff_wo[l as usize] = new_reach_diff_wo_l;
        });

        let new_can_reach_u_wo_newl = &can_reach_u_wo[newl.id as usize];
        let new_can_reach_v_wo_newl = &can_reach_v_wo[newl.id as usize];
        let new_reach_diff_wo_newl = (union != *new_can_reach_u_wo_newl) as Vertex
            + (union != *new_can_reach_v_wo_newl) as Vertex;
        new_can_reach_u_wo[newl.id as usize] = new_can_reach_u_wo_newl.clone();
        new_can_reach_v_wo[newl.id as usize] = new_can_reach_v_wo_newl.clone();
        new_useless_labels += (new_reach_diff_wo_newl == 0) as Vertex;
        new_reach_diff_wo[newl.id as usize] = new_reach_diff_wo_newl;

        if union.contains_all() {
            new_not_fully_reachable -=
                (!can_reach_u.contains_all()) as Vertex + (!can_reach_v.contains_all()) as Vertex;
        }

        u_rties.can_reach.push(union.clone());
        v_rties.can_reach.push(union);

        u_rties.can_reach_wo.push(new_can_reach_u_wo);
        v_rties.can_reach_wo.push(new_can_reach_v_wo);

        self.reach_diff_wo.push(new_reach_diff_wo);

        self.useless_labels.push(new_useless_labels);
        self.not_fully_reachable.push(new_not_fully_reachable);
    }

    pub fn pop_label(&mut self, popl: Label) {
        let (u, v) = popl.edge.ends();

        self.not_fully_reachable.pop();
        self.useless_labels.pop();

        self.mem_reuse_bvertex
            .push(self.reach_diff_wo.pop().unwrap());

        let u_rties = &mut self.vertex_reachabilities[u as usize];
        u_rties.can_reach.pop();
        self.mem_reuse_brs.push(u_rties.can_reach_wo.pop().unwrap());

        let v_rties = &mut self.vertex_reachabilities[v as usize];
        v_rties.can_reach.pop();
        self.mem_reuse_brs.push(v_rties.can_reach_wo.pop().unwrap());
    }
}
