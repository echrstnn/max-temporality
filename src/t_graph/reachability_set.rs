use crate::{generator::Automorphism, Edge, Vertex};
use itertools::Itertools;
use std::{fmt::Display, vec};

// Trait qui représente l'ensemble des sommets pouvant accéder à un sommet donné
pub trait ReachabilitySet: Clone + Eq + Display + Send + Sync + 'static {
    // Initialise un ReachabilitySet à un singleton u
    fn singleton(u: Vertex, n: Vertex) -> Self;

    // Calcule l'union de deux RS
    fn union(&self, other: &Self) -> Self;

    // Teste si un RS contient les n sommets
    fn contains_all(&self) -> bool;

    // Renvoie l'ensemble des arêtes du graphe sous-jacent dans lequel le RS est valide
    fn edges(n: Vertex) -> Vec<Edge>;

    // Renvoie l'ensemble des automorphismes du graphe sous-jacent dans lequel le RS est
    // valide. Utile pour initialiser le groupe des automorphismes lors de la génération
    fn automorphisms(n: Vertex) -> Vec<Automorphism>;
}

//
// Ancienne implémentation de CycleReachabilitySet (cf. circ_segment.rs)
//

// #[derive(Clone, PartialEq, Eq, Debug)]
// pub struct CycleReachabilitySet(CircSegment);

// impl ReachabilitySet for CycleReachabilitySet {
//     fn singleton(u: Vertex, n: Vertex) -> Self {
//         Self(CircSegment::singleton(u as isize, n as usize))
//     }

//     fn union(&self, other: &Self) -> Self {
//         Self(self.0.union(other.0))
//     }

//     fn contains_all(&self) -> bool {
//         self.0.contains_all()
//     }

//     fn edges(n: Vertex) -> Vec<Edge> {
//         (0..n).map(|u| Edge::new(u, (u + 1) % n)).collect()
//     }

//     fn automorphisms(n: Vertex) -> Vec<Automorphism> {
//         let it1 = (0..n)
//             .map(|ofs| Automorphism::new(n, (0..n).map(|u| (u + ofs) % n).collect::<Vec<_>>()));
//         let it2 = (0..n).map(|ofs| {
//             Automorphism::new(n, (0..n).map(|u| ((n + ofs) - u) % n).collect::<Vec<_>>())
//         });
//         it1.chain(it2).collect()
//     }
// }

// impl Display for CycleReachabilitySet {
//     fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
//         self.0.fmt(f)
//     }
// }

//
// ReachabilitySet pour les graphes cycles
//

#[derive(Clone, PartialEq, Eq, Debug)]
pub struct CycleReachabilitySet(u16);

impl ReachabilitySet for CycleReachabilitySet {
    fn singleton(u: Vertex, n: Vertex) -> Self {
        debug_assert!(n < 16);
        let empty = u16::MAX ^ ((1_u16 << n) - 1);
        Self(empty | (1 << u))
    }

    fn union(&self, other: &Self) -> Self {
        Self(self.0 | other.0)
    }

    fn contains_all(&self) -> bool {
        self.0 == u16::MAX
    }

    fn edges(n: Vertex) -> Vec<Edge> {
        (0..n).map(|u| Edge::new(u, (u + 1) % n)).collect()
    }

    fn automorphisms(n: Vertex) -> Vec<Automorphism> {
        let it1 = (0..n)
            .map(|ofs| Automorphism::new(n, (0..n).map(|u| (u + ofs) % n).collect::<Vec<_>>()));
        let it2 = (0..n).map(|ofs| {
            Automorphism::new(n, (0..n).map(|u| ((n + ofs) - u) % n).collect::<Vec<_>>())
        });
        it1.chain(it2).collect()
    }
}

impl Display for CycleReachabilitySet {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.contains_all() {
            "A".fmt(f)
        } else {
            for i in 0..u16::BITS {
                if (1 << i) & self.0 == 0 {
                    "_".fmt(f).unwrap();
                } else {
                    "x".fmt(f).unwrap();
                }
            }
            std::fmt::Result::Ok(())
        }
    }
}

//
// ReachabilitySet pour les graphes quelconques
//

#[derive(Clone, PartialEq, Eq, Debug)]
pub struct CompleteReachabilitySet(u16);

impl ReachabilitySet for CompleteReachabilitySet {
    fn singleton(u: Vertex, n: Vertex) -> Self {
        debug_assert!(n < 16);
        let empty = u16::MAX ^ ((1_u16 << n) - 1);
        Self(empty | (1 << u))
    }

    fn union(&self, other: &Self) -> Self {
        Self(self.0 | other.0)
    }

    fn contains_all(&self) -> bool {
        self.0 == u16::MAX
    }

    fn edges(n: Vertex) -> Vec<Edge> {
        let mut ans = vec![];
        for i in 0..n {
            for j in 0..n {
                if i < j {
                    ans.push(Edge::new(i, j));
                }
            }
        }
        ans
    }

    fn automorphisms(n: Vertex) -> Vec<Automorphism> {
        (0..n)
            .permutations(n as usize)
            .map(|p| Automorphism::new(n, p))
            .collect()
    }
}

impl Display for CompleteReachabilitySet {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.contains_all() {
            "A".fmt(f)
        } else {
            for i in 0..u16::BITS {
                if (1 << i) & self.0 == 0 {
                    "_".fmt(f).unwrap();
                } else {
                    "x".fmt(f).unwrap();
                }
            }
            std::fmt::Result::Ok(())
        }
    }
}
