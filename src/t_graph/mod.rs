mod reachabilities;
mod reachability_set;

// Alias sur les types
pub type TGraph = generic::TGraph<generic::CompleteReachabilitySet>;
pub type TCycleGraph = generic::TGraph<generic::CycleReachabilitySet>;

//
// Module contenant une implémentation générique paramétrée par le type ReachabilitySet
// d'un graphe temporel
//

pub mod generic {
    use rustc_hash::{FxHashMap, FxHashSet};
    use std::cmp::max;
    use std::ops::Range;

    use super::reachabilities::*;
    pub use super::reachability_set::*;
    use crate::{Edge, Label, LabelId, LabelTime, Vertex};

    #[derive(Clone)]
    pub struct TGraph<RS: ReachabilitySet> {
        // Nombre de sommets
        n: Vertex,

        // Arêtes du graphe sous-jacent (certaines arêtes peuvent ne pas avoir de label)
        edges: FxHashSet<Edge>,

        // Listes d'adjacence du graphe sous-jacent
        adj: Vec<Vec<Edge>>,

        // Pile des labels ajoutés
        labels: Vec<Label>,

        // Pile des labels ajoutés sur une arête donnée
        labels_on: FxHashMap<Edge, Vec<Label>>,

        // Prochain label à ajouter
        next_time: LabelTime,

        // Structure qui maintient les accessibilités et permet les tests de connexité
        // et minimalités en temps constant moyennant un surcoût en O(labels.len()) lors
        // de push_label et pop_label
        reach: Reachabilities<RS>,

        // Pile dont le sommet est la temporalité du graphe
        temporality: Vec<LabelTime>,
    }

    impl<RS: ReachabilitySet> TGraph<RS> {
        // Initialise un nouveau graphe temporel. Le graphe sous-jacent est récupéré via
        // l'implémentation du ReachabilitySet
        pub fn new(n: Vertex) -> Self {
            let edges: FxHashSet<Edge> = RS::edges(n).into_iter().collect();
            let mut adj = vec![vec![]; n as usize];
            edges.iter().for_each(|&uv| {
                let (u, v) = uv.ends();
                adj[u as usize].push(uv);
                adj[v as usize].push(uv);
            });
            let labels_on = edges.iter().map(|&uv| (uv, vec![])).collect();
            Self {
                n,
                edges,
                adj,
                labels: vec![],
                labels_on,
                next_time: 0,
                reach: Reachabilities::new(n),
                temporality: vec![0],
            }
        }

        pub fn vertices(&self) -> Range<Vertex> {
            0..self.n as Vertex
        }

        pub fn edges(&self) -> impl Iterator<Item = &Edge> + '_ {
            self.edges.iter()
        }

        pub fn adj_edges(&self, u: Vertex) -> impl Iterator<Item = &Edge> + '_ {
            self.adj[u as usize].iter()
        }

        pub fn labels(&self) -> impl Iterator<Item = &Label> + '_ {
            self.labels.iter()
        }

        pub fn labels_on(&self, uv: Edge) -> impl Iterator<Item = &Label> + '_ {
            self.labels_on.get(&uv).unwrap().iter()
        }

        pub fn next_time(&self) -> LabelTime {
            self.next_time
        }

        pub fn temporality(&self) -> LabelTime {
            *self.temporality.last().unwrap()
        }

        //
        // Tests d'accessibilité
        //

        // Renvoie l'ensemble des sommets pouvant accéder à u
        pub fn reachability_set(&self, u: Vertex) -> &RS {
            self.reach.reachability_set(u)
        }

        pub fn is_useless_label(&self, l: Label) -> bool {
            self.reach.is_useless_label(l)
        }

        pub fn is_temporally_connected(&self) -> bool {
            self.reach.is_temporally_connected()
        }

        pub fn is_minimal(&self) -> bool {
            self.reach.is_minimal()
        }

        //
        // push_label et pop_label
        //

        pub fn incr_time(&mut self) {
            self.next_time += 1;
        }

        pub fn decr_time(&mut self) {
            self.next_time -= 1;
        }

        pub fn push_label(&mut self, uv: Edge) {
            let new_l = Label {
                id: self.labels.len() as LabelId,
                time: self.next_time,
                edge: uv,
            };

            debug_assert!(
                new_l.time == self.labels.last().map(|l| l.time).unwrap_or(0)
                    || new_l.time == self.labels.last().map(|l| l.time).unwrap_or(0) + 1
            );
            debug_assert!(self
                .labels()
                .filter(|&l| l.time == new_l.time)
                .all(|&l| !l.edge.adjacent(new_l.edge)));

            // On met a jour les accessibilités
            self.reach.push_label(new_l);

            // On met a jour la pile des labels
            self.labels.push(new_l);

            // On met a jour les labels sur les arêtes et la temporalité
            let temporality = self.temporality();
            let labels_on_uv = self.labels_on.get_mut(&uv).unwrap();
            labels_on_uv.push(new_l);
            self.temporality
                .push(max(temporality, labels_on_uv.len() as LabelTime));
        }

        pub fn pop_label(&mut self) {
            let l = self.labels.pop().unwrap();

            // On met a jour les labels sur les arêtes et la temporalité
            self.labels_on.get_mut(&l.edge).unwrap().pop();
            self.temporality.pop();

            // On met a jour les accessibilités
            self.reach.pop_label(l);
        }
    }
}
