use crate::{generic, Edge, Vertex};
use std::{
    fs::{create_dir, remove_dir_all, File},
    process::Command,
};

// Structure qui permet de produire des .dot pour afficher un TGraph avec GraphViz
pub struct TGraphPlotter {
    // Répertoire de travail
    dir: String,

    // Fichier .dot
    f: File,
}

fn exec_cmd(cmd: String) {
    Command::new("/bin/bash")
        .arg("-c")
        .arg(cmd)
        .spawn()
        .unwrap()
        .wait()
        .unwrap();
}

#[allow(dead_code)]
impl TGraphPlotter {
    // Initialise un TGraphPlotter dans le répertoire dir
    // ATTENTION: supprime l'ancien contenu du réperoire
    pub fn new(dir: &str) -> Self {
        let _ = remove_dir_all(dir);
        create_dir(dir).unwrap();
        Self {
            dir: String::from(dir),
            f: File::create(format!("{}/plot.dot", dir)).unwrap(),
        }
    }

    // Ajoute la description du graphe g à la fin du fichier .dot
    pub fn plot<RS: generic::ReachabilitySet>(&mut self, g: &generic::TGraph<RS>) {
        dot::render(g, &mut self.f).unwrap();
    }
}

impl Drop for TGraphPlotter {
    fn drop(&mut self) {
        self.f.sync_all().unwrap();
        exec_cmd(format!("./render_plots.sh {}", self.dir));
    }
}

//
// Utilisation de la crate dot pour produire les descriptions .dot des TGraph
//

impl<'a, RS: generic::ReachabilitySet> dot::Labeller<'a, Vertex, Edge> for generic::TGraph<RS> {
    fn graph_id(&'a self) -> dot::Id<'a> {
        dot::Id::new("g").unwrap()
    }

    fn node_id(&'a self, n: &Vertex) -> dot::Id<'a> {
        dot::Id::new(format!("u{}", *n)).unwrap()
    }

    fn node_label(&'a self, n: &Vertex) -> dot::LabelText<'a> {
        dot::LabelText::label(format!("{}", n))
        // Pour afficher les ReachabilitySet sur les sommets
        // dot::LabelText::label(format!("{}\n{}", n, self.reachability_set(*n)))
    }

    fn node_color(&'a self, _node: &Vertex) -> Option<dot::LabelText<'a>> {
        if self.is_temporally_connected() {
            Some(dot::LabelText::label("green"))
        } else {
            None
        }
    }

    fn edge_label(&'a self, e: &Edge) -> dot::LabelText<'a> {
        let labels_str = self
            .labels_on(*e)
            .map(|&l| {
                if self.is_useless_label(l) {
                    format!("({})", l.time)
                } else {
                    format!("{}", l.time)
                }
            })
            .collect::<Vec<_>>()
            .join(",");
        dot::LabelText::label(labels_str)
    }

    fn edge_color(&'a self, _e: &Edge) -> Option<dot::LabelText<'a>> {
        if self.is_minimal() {
            None
        } else {
            Some(dot::LabelText::label("red"))
        }
    }

    fn edge_style(&'a self, _e: &Edge) -> dot::Style {
        if self.labels_on(*_e).next().is_none() {
            dot::Style::Dotted
        } else {
            dot::Style::None
        }
    }

    fn kind(&self) -> dot::Kind {
        dot::Kind::Graph
    }
}

impl<'a, RS: generic::ReachabilitySet> dot::GraphWalk<'a, Vertex, Edge> for generic::TGraph<RS> {
    fn nodes(&self) -> dot::Nodes<'a, Vertex> {
        let nodes = self.vertices().collect::<Vec<_>>();
        std::borrow::Cow::Owned(nodes)
    }

    fn edges(&'a self) -> dot::Edges<'a, Edge> {
        let edges = self.edges().cloned().collect::<Vec<_>>();
        std::borrow::Cow::Owned(edges)
    }

    fn source(&self, e: &Edge) -> Vertex {
        e.ends().0
    }

    fn target(&self, e: &Edge) -> Vertex {
        e.ends().1
    }
}
