use super::{Callback, GeneratorThread};
use crate::{generic, Edge};

struct MatchingGenGlobals {
    free_vertex: Vec<bool>,
    matching: Vec<Edge>,
}

struct MatchingGenParams<'a> {
    remaining: &'a [Edge],
    auts: Vec<usize>,
    checkpoint: u8,
}

impl<F, D, RS> GeneratorThread<F, D, RS>
where
    F: Callback<RS>,
    D: Callback<RS>,
    RS: generic::ReachabilitySet,
{
    pub fn for_each_matching<T>(&mut self, eligible: Vec<Edge>, mut f: T)
    where
        T: FnMut(&mut Self, &Vec<Edge>),
    {
        let mut globals = MatchingGenGlobals {
            free_vertex: vec![true; self.G.vertices().len()],
            matching: Vec::with_capacity(eligible.len() / 2),
        };
        let params = MatchingGenParams {
            remaining: &eligible[..],
            auts: self.auts.last().unwrap().clone(),
            checkpoint: 0,
        };
        let mut res = self.for_each_matching_(&mut globals, &mut f, params);
        while let Some(stack_save) = res {
            println!("Restore stack...");
            res = self.restore_stack(&mut globals, &mut f, stack_save);
        }
    }

    fn for_each_matching_<'a, T>(
        &mut self,
        globals: &mut MatchingGenGlobals,
        f: &mut T,
        params: MatchingGenParams<'a>,
    ) -> Option<Vec<MatchingGenParams<'a>>>
    where
        T: FnMut(&mut Self, &Vec<Edge>),
    {
        if self.G.is_minimal() {
            // Si reste des arêtes à potentiellement ajouter
            if let Some(&uv) = params.remaining.first() {
                let (u, v) = uv.ends();
                if globals.free_vertex[u] && globals.free_vertex[v] {
                    globals.free_vertex[u] = false;
                    globals.free_vertex[v] = false;
                    globals.matching.push(uv);
                    self.G.push_label(uv);
                    self.debug();

                    return self.checkpoint0(globals, f, params);
                } else {
                    return self.checkpoint1(globals, f, params);
                }
            }
            // Sinon si le couplage est non vide et est isomorphe à aucun autre vu
            else if !globals.matching.is_empty() && params.auts.is_empty() {
                self.G.incr_time();
                f(self, &globals.matching);
                self.G.decr_time();
                return Some(vec![]);
            }
        }
        None
    }

    fn restore_stack<'a, T>(
        &mut self,
        globals: &mut MatchingGenGlobals,
        f: &mut T,
        mut stack_save: Vec<MatchingGenParams<'a>>,
    ) -> Option<Vec<MatchingGenParams<'a>>>
    where
        T: FnMut(&mut Self, &Vec<Edge>),
    {
        let params = stack_save.pop().unwrap();
        if let Some(new_params) = stack_save.pop() {
            let new_params = self.new_params(&params, new_params.checkpoint);
            stack_save.push(new_params);
            self.restore_stack(globals, f, stack_save);
        }

        debug_assert!(params.checkpoint == 0 || params.checkpoint == 1);
        if params.checkpoint == 0 {
            self.checkpoint0(globals, f, params)
        } else {
            self.checkpoint1(globals, f, params)
        }
    }

    fn new_params<'a>(
        &mut self,
        params: &MatchingGenParams<'a>,
        new_checkpoint: u8,
    ) -> MatchingGenParams<'a> {
        debug_assert!(params.checkpoint == 0 || params.checkpoint == 1);
        if params.checkpoint == 0 {
            let (u, v) = params.remaining.first().unwrap().ends();
            MatchingGenParams {
                remaining: &params.remaining[1..],
                auts: params
                    .auts
                    .iter()
                    .filter(|&&id| {
                        (self.all_auts[id][u] == u && self.all_auts[id][v] == v)
                            || (self.all_auts[id][u] == v && self.all_auts[id][v] == u)
                    })
                    .cloned()
                    .collect(),
                checkpoint: new_checkpoint,
            }
        } else {
            MatchingGenParams {
                remaining: &params.remaining[1..],
                auts: params.auts.clone(),
                checkpoint: new_checkpoint,
            }
        }
    }

    fn checkpoint0<'a, T>(
        &mut self,
        globals: &mut MatchingGenGlobals,
        f: &mut T,
        params: MatchingGenParams<'a>,
    ) -> Option<Vec<MatchingGenParams<'a>>>
    where
        T: FnMut(&mut Self, &Vec<Edge>),
    {
        let (u, v) = params.remaining.first().unwrap().ends();
        let new_params = self.new_params(&params, 0);

        if let Some(mut stack_save) = self.for_each_matching_(globals, f, new_params) {
            stack_save.push(params);
            return Some(stack_save);
        }

        self.G.pop_label();
        self.debug();
        globals.matching.pop();
        globals.free_vertex[u] = true;
        globals.free_vertex[v] = true;

        self.checkpoint1(globals, f, params)
    }

    fn checkpoint1<'a, T>(
        &mut self,
        globals: &mut MatchingGenGlobals,
        f: &mut T,
        params: MatchingGenParams<'a>,
    ) -> Option<Vec<MatchingGenParams<'a>>>
    where
        T: FnMut(&mut Self, &Vec<Edge>),
    {
        let new_params = self.new_params(&params, 0);
        self.for_each_matching_(globals, f, new_params)
            .map(|mut stack_save| {
                stack_save.push(params);
                stack_save
            })
    }
}
