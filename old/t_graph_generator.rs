use crate::{Edge, TCycleGraph};

#[allow(non_snake_case)]
pub struct TGraphGenerator<F, D>
where
    F: FnMut(&TCycleGraph),
    D: FnMut(&TCycleGraph),
{
    G: TCycleGraph,
    n_vertices: usize,
    edges: Vec<Edge>,
    callback: F,
    debug_callback: D,
    debug: bool,
}

impl<F, D> TGraphGenerator<F, D>
where
    F: FnMut(&TCycleGraph),
    D: FnMut(&TCycleGraph),
{
    pub fn new(initial: TCycleGraph, callback: F, debug_callback: D, debug: bool) -> Self {
        let n_vertices = initial.vertices().len();
        let edges = initial.edges().cloned().collect();
        Self {
            G: initial,
            n_vertices,
            edges,
            callback,
            debug_callback,
            debug,
        }
    }

    fn circ_order_key(uv: &Edge) -> usize {
        let (u, v) = uv.ends();
        if u == 0 && v != 1 {
            u
        } else {
            v
        }
    }

    fn callback(&mut self) {
        (self.callback)(&self.G);
    }

    fn debug(&mut self) {
        if self.debug {
            (self.debug_callback)(&self.G);
        }
    }

    fn generate_matchings_lin(&mut self, eligible_edges: &[Edge], matching: &mut Vec<Edge>) {
        if self.G.is_minimal() {
            if let Some((&uv, wo_uv)) = eligible_edges.split_first() {
                matching.push(uv);
                self.G.push_label(uv);
                self.debug();

                let (_, v) = uv.ends();
                let new_eligible = match wo_uv.split_first() {
                    Some((&uvnext, wo_uv_uvnext)) if uvnext == Edge::new(v, v + 1) => wo_uv_uvnext,
                    _ => wo_uv,
                };

                self.generate_matchings_lin(new_eligible, matching);

                self.G.pop_label();
                matching.pop();
                self.debug();

                self.generate_matchings_lin(wo_uv, matching);
            } else {
                self.G.incr_time();
                self.generate_tgraphs(matching);
                self.G.decr_time();
            }
        }
    }

    fn generate_matchings_circ(&mut self, eligible_edges: &[Edge]) {
        let mut matching = vec![];
        if let Some((&first_eligible, wo_first)) = eligible_edges.split_first() {
            if first_eligible == Edge::new(self.n_vertices - 1, 0) {
                matching.push(first_eligible);
                self.G.push_label(first_eligible);
                self.debug();

                let mut new_eligible = match wo_first.split_first() {
                    Some((&snd, wo_first_snd)) if snd == Edge::new(0, 1) => wo_first_snd,
                    _ => wo_first,
                };
                new_eligible = match new_eligible.split_last() {
                    Some((&last, wo_last))
                        if last == Edge::new(self.n_vertices - 2, self.n_vertices - 1) =>
                    {
                        wo_last
                    }
                    _ => new_eligible,
                };

                self.generate_matchings_lin(new_eligible, &mut matching);

                self.G.pop_label();
                matching.pop();
                self.debug();

                self.generate_matchings_lin(wo_first, &mut matching);
            } else {
                self.generate_matchings_lin(eligible_edges, &mut matching);
            }
        }
    }

    fn generate_tgraphs(&mut self, recent_edges: &Vec<Edge>) {
        if self.G.is_minimal() {
            if self.G.is_temporally_connected() {
                self.callback();
            } else {
                let mut eligible_edges = recent_edges
                    .iter()
                    .flat_map(|&uv| {
                        let (u, v) = uv.ends();
                        if uv == Edge::new(self.n_vertices - 1, 0) {
                            [
                                Edge::new(self.n_vertices - 2, self.n_vertices - 1),
                                uv,
                                Edge::new(0, 1),
                            ]
                        } else if uv == Edge::new(0, 1) {
                            [Edge::new(self.n_vertices - 1, 0), uv, Edge::new(1, 2)]
                        } else {
                            [
                                Edge::new(u - 1, u),
                                uv,
                                Edge::new(v, (v + 1) % self.n_vertices),
                            ]
                        }
                    })
                    .filter(|&uv| {
                        let (u, v) = uv.ends();
                        self.G.reachability_set(u) != self.G.reachability_set(v)
                    })
                    .collect::<Vec<_>>();
                eligible_edges.sort_by_key(Self::circ_order_key);
                eligible_edges.dedup();
                self.generate_matchings_circ(eligible_edges.as_slice());
            }
        }
    }

    pub fn generate(&mut self) {
        let mut edges = self.edges.clone();
        edges.sort_unstable_by_key(Self::circ_order_key);

        let e_n1_0 = Edge::new(self.n_vertices - 1, 0);
        let mut matching = vec![e_n1_0];
        self.G.push_label(e_n1_0);
        self.debug();
        self.generate_matchings_lin(&edges[2..edges.len() - 1], &mut matching);
    }
}
