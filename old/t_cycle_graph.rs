use crate::{CircSegment, Edge};
use rustc_hash::{FxHashMap, FxHashSet};
use std::ops::Range;

type AccessTable = Vec<Vec<CircSegment>>;

pub struct TCycleGraph {
    adj_edges: Vec<FxHashSet<Edge>>,
    edges: FxHashSet<Edge>,
    labels_on_edge: FxHashMap<Edge, Vec<(usize, usize)>>,
    labels: Vec<Edge>,
    next_label: usize,
    can_access_wo: Vec<AccessTable>,
    n_access_diffenrences_wo: Vec<usize>,
    n_unecessary_labels: usize,
    n_not_fully_accessible: usize,
    temporality: Vec<usize>,
}

impl TCycleGraph {
    pub fn new(n_vertices: usize) -> TCycleGraph {
        debug_assert!(n_vertices >= 2);
        let initial_table = (0..n_vertices)
            .map(|u| vec![CircSegment::singleton(u as isize, n_vertices)])
            .collect();
        let mut g = TCycleGraph {
            adj_edges: (0..n_vertices).map(|_| FxHashSet::default()).collect(),
            edges: FxHashSet::default(),
            labels_on_edge: FxHashMap::default(),
            labels: vec![],
            next_label: 1,
            can_access_wo: vec![initial_table],
            n_access_diffenrences_wo: vec![0],
            n_unecessary_labels: 0,
            n_not_fully_accessible: n_vertices,
            temporality: vec![0],
        };
        (0..n_vertices).for_each(|u| g.add_edge(Edge::new(u, (u + 1) % n_vertices)));
        g
    }

    pub fn vertices(&self) -> Range<usize> {
        0..self.adj_edges.len()
    }

    pub fn edges(&self) -> &FxHashSet<Edge> {
        &self.edges
    }

    pub fn labels(&self) -> &Vec<Edge> {
        &self.labels
    }

    fn add_edge(&mut self, uv: Edge) {
        let (u, v) = uv.ends();
        self.adj_edges[u].insert(uv);
        self.adj_edges[v].insert(uv);
        self.edges.insert(uv);
        self.labels_on_edge.insert(uv, vec![]);
    }

    pub fn get_labels_on_edge(&self, uv: Edge) -> impl Iterator<Item = &(usize, usize)> + '_ {
        self.labels_on_edge.get(&uv).unwrap().iter()
    }

    pub fn push_time(&mut self) {
        self.next_label += 1
    }

    pub fn pop_time(&mut self) {
        self.next_label -= 1;
    }

    pub fn push_label(&mut self, uv: Edge) {
        self.labels.push(uv);

        // Update temporality
        let old_temporality = self.temporality();
        let labels_on_uv = self.labels_on_edge.get_mut(&uv).unwrap();
        labels_on_uv.push((self.next_label, self.labels.len()));
        let new_temporality = std::cmp::max(old_temporality, labels_on_uv.len());
        self.temporality.push(new_temporality);

        // Update access tables
        let (u, v) = uv.ends();
        let old_can_access_u = *self.can_access_wo[0][u].last().unwrap();
        let old_can_access_v = *self.can_access_wo[0][v].last().unwrap();
        let can_access_wo_newlabel = self.can_access_wo[0]
            .iter()
            .map(|can_access_u| vec![*can_access_u.last().unwrap()])
            .collect::<Vec<_>>();
        (0..self.labels().len()).for_each(|label| {
            let can_access_wo_label = &mut self.can_access_wo[label];
            let old_can_access_u_wo_label = *can_access_wo_label[u].last().unwrap();
            let old_can_access_v_wo_label = *can_access_wo_label[v].last().unwrap();
            let union_wo_label = old_can_access_u_wo_label.union(old_can_access_v_wo_label);
            can_access_wo_label[u].push(union_wo_label);
            can_access_wo_label[v].push(union_wo_label);
            let union = *self.can_access_wo[0][u].last().unwrap();

            let diff = &mut self.n_access_diffenrences_wo[label];
            let old_diff = *diff;
            *diff += 2 * (union_wo_label != union) as usize;
            *diff -= (old_can_access_u_wo_label != old_can_access_u) as usize;
            *diff -= (old_can_access_v_wo_label != old_can_access_v) as usize;
            if old_diff != 0 && *diff == 0 {
                self.n_unecessary_labels += 1;
            }
        });

        // Push the new access table
        self.can_access_wo.push(can_access_wo_newlabel);
        let union = *self.can_access_wo[0][u].last().unwrap();
        let diff = (union != old_can_access_u) as usize + (union != old_can_access_v) as usize;
        self.n_access_diffenrences_wo.push(diff);
        if diff == 0 {
            self.n_unecessary_labels += 1;
        }

        if let CircSegment::FullSet(_) = union {
            self.n_not_fully_accessible -=
                (old_can_access_u != union) as usize + (old_can_access_v != union) as usize;
        }
    }

    pub fn pop_label(&mut self) {
        let uv = self.labels.last().unwrap();

        let (u, v) = uv.ends();
        let union = *self.can_access_wo[0][u].last().unwrap();
        debug_assert_eq!(union, *self.can_access_wo[0][v].last().unwrap());

        let old_can_access_u = self.can_access_wo[0][u][self.can_access_wo[0][u].len() - 2];
        let old_can_access_v = self.can_access_wo[0][v][self.can_access_wo[0][v].len() - 2];

        if let CircSegment::FullSet(_) = union {
            self.n_not_fully_accessible +=
                (old_can_access_u != union) as usize + (old_can_access_v != union) as usize;
        }

        if self.n_access_diffenrences_wo.pop().unwrap() == 0 {
            self.n_unecessary_labels -= 1;
        }
        self.can_access_wo.pop();

        (0..self.labels().len()).for_each(|label| {
            let can_access_wo_label = &mut self.can_access_wo[label];
            let union_wo_label = can_access_wo_label[u].pop().unwrap();
            debug_assert_eq!(union_wo_label, *can_access_wo_label[v].last().unwrap());
            can_access_wo_label[v].pop();

            let old_can_access_u_wo_label = *can_access_wo_label[u].last().unwrap();
            let old_can_access_v_wo_label = *can_access_wo_label[v].last().unwrap();

            let old_diff = &mut self.n_access_diffenrences_wo[label];
            let diff = *old_diff;
            *old_diff += (old_can_access_u_wo_label != old_can_access_u) as usize;
            *old_diff += (old_can_access_v_wo_label != old_can_access_v) as usize;
            *old_diff -= 2 * (union_wo_label != union) as usize;

            if *old_diff != 0 && diff == 0 {
                self.n_unecessary_labels -= 1;
            }
        });

        self.temporality.pop();
        self.labels_on_edge.get_mut(uv).unwrap().pop();
        self.labels.pop();
    }

    pub fn get_vertices_can_acces(&self, u: usize) -> CircSegment {
        *self.can_access_wo[0][u].last().unwrap()
    }

    pub fn is_label_necessary(&self, label: usize) -> bool {
        self.n_access_diffenrences_wo[label] > 0
    }

    pub fn is_minimal(&self) -> bool {
        self.n_unecessary_labels == 0
    }

    pub fn is_temporally_connected(&self) -> bool {
        self.n_not_fully_accessible == 0
    }

    pub fn temporality(&self) -> usize {
        *self.temporality.last().unwrap()
    }
}
